const express = require('express');
const app = express();
const router = express.Router();
const bodyParser = require("body-parser");
const result = require("../modal/dbConnect");

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

const users = [
    {
        id: 1,
        username: 'Cenkay',
        password: 12345
    }
];

/* GET home page. */
router.get('/', function (req, res, next) {
    /*let sorgu = {};
    db.connection('listingsAndReviews').findOne(sorgu, (err, results) => {
        if(err) throw err;
        console.log(results);
    });*/
    //res.render('index', { title: 'Express' });
    res.status(200).jsonp({
        data: result
    })
});
router.post('/', (req, res) => {
    users.forEach((user) => {
        if (user.username === req.query.username) {
            res.status(200).jsonp({
                data: user
            })
        } else {
            res.status(401).json({
                data: 'Kullanıcı Bulunamdı!'
            })
        }
    })
});

module.exports = router;
